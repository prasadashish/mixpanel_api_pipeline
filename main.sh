### This bash script calls all the other scripts one by one to execute it sequentially.
python Data_Download_s3.py &&
python mixpanel_data_pipeline.py &&      ### pulls only PayPal, Bitcoin price and savedroid trends data
#python Dashboard_ver_1_3.py && ### file to convert html from ipyhton 
python Data_Upload_s3.py             ### uploads the data into "ds-dashboard-data" S3 bucket
