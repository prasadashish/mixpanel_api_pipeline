#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 14:34:15 2019

@author: sah
"""

import time
import os
import json
import datetime
import numpy as np
import pandas as pd
from mixpanel_api import Mixpanel
from datetime import datetime as dt
from datetime import date, timedelta
from mixpanel_api import Mixpanel
from plotly import graph_objs as go
from collections import Counter

## get credentials for Mixpanel (Gautam's credentials are used)
with open('./keys/Mixpanel_Credentials.json') as json_file:
    Mixpanel_Credentials = json.load(json_file)
    
nowraw = datetime.datetime.now()
two_daysraw = nowraw - datetime.timedelta(days= 1)
three_daysraw = nowraw - datetime.timedelta(days= 2)


## pull data from iOS crypto app of Mixpanel
## Export dataset by "people" from Mixpanel for conversionfunnel(), activedevices() graphs 
api_secret_ios = Mixpanel_Credentials['api_secretiOS']
token_ios = Mixpanel_Credentials['tokeniOS']
## pull data from Androd crypto app of Mixpanel
api_secret_android = Mixpanel_Credentials['api_secret_Android']
token_android = Mixpanel_Credentials["token_Android"]


#conver time
def convert_time(var):
    x = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(var))
    return x
def convert_processing_time(var):
    x = dt.fromtimestamp(var/1000).strftime('%Y-%m-%d %H:%M:%S')
    return x

if not os.path.exists(os.getcwd()+"/Android"):
    os.mkdir(os.getcwd()+"/Android")
    os.mkdir(os.getcwd()+"/Android/events")
    os.mkdir(os.getcwd()+"/Android/events/Data")
    print("Fetching all data")
    mpanel_droid = Mixpanel(api_secret_android, token = token_android)
    # Export dataset by events from Mixpanel for Android for conversionfunnel() graphs
    mpanel_droid.export_events('./Android/events/Data/events_android.json',{'from_date':str(three_daysraw.date()),'to_date':str(two_daysraw.date())})
    
    # clean the data
    df = pd.read_json('./Android/events/Data/events_android.json')
    df_ext = pd.DataFrame(df['properties'].values.tolist(), index=df.index)
    df_ext['event']= df['event']
    
    #convert the time
    df_ext["time"] = df_ext["time"].apply(lambda x: convert_time(x))
    df_ext["mp_processing_time_ms"] = df_ext["mp_processing_time_ms"].apply(lambda x: convert_processing_time(int(x)))
    
    #remove irrelevant columns
    #Checking the relevance of column based on how much a column has legitimate data(by removing unknown, none and null values)
    col_rel = df_ext.apply(lambda c: (c.replace(['Unknown', 'None', ''], np.nan).notna().sum() / c.size) * 100).sort_values()    
    irr_col = col_rel.where(col_rel<10).dropna().index  
    irr_col = list(irr_col)
    irr_col.extend(['$app_release', '$app_version_string', '$brand', '$google_play_services', '$os', 'Model Name', 'mp_lib'])    
    df_ext = df_ext.drop(irr_col, axis=1)
    df_ext["time"]=pd.to_datetime(df_ext.time)
    #df_ext=df_ext.set_index('time')
    df_ext.to_json("./Android/events/Data/mixpanel_event_data.json")
    irr_col_df =pd.DataFrame({'irrelevant_columns':irr_col})
    irr_col_df.to_json("./Android/events/Data/irrelevant_columns.json")
    os.remove("./Android/events/Data/events_android.json")
    
else:
    print("Fetching today's data(until now)")
     #read_archived data
    df_old = pd.read_json("./Android/events/Data/mixpanel_event_data.json")
    df_old["time"] = df_old["time"].apply(lambda x: convert_processing_time(int(x)))
    df_old["time"]=pd.to_datetime(df_old.time)    
    df_old=df_old.set_index("time")
    from_data = pd.to_datetime(df_old.index[-1]).date()
    df_old=df_old.loc[:from_data]
    df_old=df_old.reset_index()
    mpanel_droid = Mixpanel(api_secret_android, token = token_android)
    # Export dataset by events from Mixpanel for Android for conversionfunnel() graphs
    mpanel_droid.export_events('./Android/events/Data/events_android.json',{'from_date':from_data,'to_date':str(nowraw.date())})
    #read data
    df_today = pd.read_json('./Android/events/Data/events_android.json')
    df_ext_today = pd.DataFrame(df_today['properties'].values.tolist(), index=df_today.index)
    df_ext_today['event']= df_today['event']
    irr_col_df = pd.read_json("./Android/events/Data/irrelevant_columns.json")    
    #convert the time
    df_ext_today["time"] = df_ext_today["time"].apply(lambda x: convert_time(x))
    
    df_ext_today["mp_processing_time_ms"] = df_ext_today["mp_processing_time_ms"].apply(lambda x: convert_processing_time(int(x)))
    irr_col =  list(set(list(irr_col_df.irrelevant_columns)).intersection(list(df_ext_today.columns)))   
    df_ext_today = df_ext_today.drop(irr_col, axis=1)
    #df_ext_today=df_ext_today.set_index("time")
    os.remove("./Android/events/Data/events_android.json")
    #convert to datetime
    #df_old["time"]=pd.to_datetime(df_old.time)
    #keep unique values
    df_unique = pd.concat([df_old,df_ext_today],sort=False).drop_duplicates().reset_index(drop=True)
    #convert to datetime
    df_unique["time"]=pd.to_datetime(df_unique.time)
    df_unique.to_json("./Android/events/Data/mixpanel_event_data.json")

