import boto3
import os

local = os.getcwd()
# create session to connect with S3 bucket named "ds-news"
#session = boto3.session.Session(profile_name='savedroid-li')   ## Switch to other AWS configuration if needed
session = boto3.session.Session()
s3 = session.resource('s3')
bucket_name = s3.Bucket("ds-mixpaneldata")

for s3_object in bucket_name.objects.all():
    path, filename = os.path.split(s3_object.key)
    filePath = local+"/"+path
    print("path:",path)
    print("filename:",filename)
    if not os.path.exists(filePath):
    	os.mkdir(filePath)
    if(len(filename)!=0):
        bucket_name.download_file(s3_object.key, filePath+"/"+filename)
print("s3 download completed successfully.")
