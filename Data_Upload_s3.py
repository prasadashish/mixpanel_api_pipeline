import boto3
import os

os.chdir("./Android")

# Create an S3 client
def send_bucket():
    s3 = boto3.client('s3')
    bucket_name = 'ds-mixpaneldata'

    location = os.getcwd()

    for path, subdirs, files in os.walk(location):
        os.chdir(path)
        for items in files:
            s3.upload_file(items, bucket_name, path[path.rfind('Android'):]+'/{}'.format(items))

# Uploads the given file using a managed uploader, which will split up large
# files automatically and upload parts in parallel.
#s3.upload_file(filename, bucket_name, filename)

if __name__== "__main__":
    send_bucket()
print("Files uploaded to S3 successfully.")
